import requests


def handler(event, context):
    res_str = "Requests version: %s" % requests.__version__
    print(res_str)
    # print('request: {}'.format(json.dumps(event)))
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'text/plain'
        },
        'body': res_str
    }
